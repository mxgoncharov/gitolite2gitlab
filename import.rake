namespace :gitlab do
  namespace :import do
    # How to use:
    #
    #  1. copy the bare repos under the repos_path (commonly /home/git/repositories)
    #  2. run: bundle exec rake gitlab:import:repos RAILS_ENV=production
    #
    # Notes:
    #  * The project owner will set to the first administator of the system
    #  * Existing projects will be skipped
    #
    task users: :environment do
      home_dir = '' # PATH TO KEYS' DIR (GITOLITE KEYS)
      keys = Dir.entries(home_dir).sort
      nicknames = keys.map { |k| k.split('.')[0] }
      nicknames.reject! { |n| n.nil? }

      ActiveRecord::Base.transaction do
        nicknames.each do |n|
          puts "Processing #{n}".yellow
          if User.find_by_username(n).present?
            puts 'Already exists'.yellow
            next
          end

          user = User.new(
            username: n,
            name: n,
            email: "#{n}@yourcompany.com", # YOUR COMPANY MAIL DOMAIN
            projects_limit: 100000,
            force_random_password: true,
            password_expires_at: nil
          )

          user.generate_password
          user.generate_reset_token
          user.skip_confirmation!
          user.save(validate: false)

          file = File.open("#{home_dir}/#{n}.pub", "rb")
          if file.present?
            ssh_key = file.read

            key = user.keys.new(title: "#{n}-key", key: ssh_key)
            key.save!

            puts "Key added".yellow
          end

          puts "Proceed #{n}".green
        end
      end
    end

    task update_users: :environment do
      users = User.all
      users.each do |user|
       user.notification_email = user.email
       if user.save(validate: false)
        puts "#{user.username} updated".green
       else
        puts "#{user.username} update failed".red
       end
      end
    end

    task delete_users: :environment do
      users = User.where(admin: false)
      users.delete_all
    end

    task groups: :environment do
      config_file = '' # PATH TO gitolite.conf FILE
      admin_username = 'root'
      admin = User.find_by_username(admin_username)

      group_lines = []

      line_num = 0
      text = File.open(config_file).read
      text.gsub!(/\r\n?/, "\n")
      text.each_line do |line|
        group_lines << line if line[0] == '@'
      end

      all_groups = {}
      group_lines.each do |group_line|
        splitted = group_line.split('=')
        users = splitted[1].split(' ').map! { |x| x.strip }
        all_groups[splitted[0].strip] =  users
      end

      all_groups.each do |g, u|
        nested_groups = u.select { |user| user[0] == '@' }
        replaced_groups = []
        nested_groups.each do |group|
          replaced_groups.concat(all_groups[group])
          replaced_groups.uniq!
        end if nested_groups.present? && nested_groups.any?
        u.concat(replaced_groups)
        u.reject! { |x| x[0] == '@' }
        u.uniq!
      end
      puts all_groups.inspect

      if all_groups.present? && all_groups.any?
        all_groups.each do |group_key, usernames|
          group_name = group_key[1..-1]
          if Group.find_by_path(group_name)
            puts "#{group_name} already exists"
          end
          group = Group.new(
            name: group_name,
            path: group_name
          )
          puts "Group #{group_name} created".green if group.save
          group.add_owner(admin)
          puts 'Adding users'.yellow
          users = User.where(username: usernames)
          group.add_users(users.map(&:id), Gitlab::Access::DEVELOPER)
          puts "Users added to #{group_name}".green
        end
      else
        puts 'Nothing to import'.yellow
      end
    end

    desc "GITLAB | Import bare repositories from gitlab_shell -> repos_path into GitLab project instance"
    task repos: :environment do

      git_base_path = Gitlab.config.gitlab_shell.repos_path
      repos_to_import = Dir.glob(git_base_path + '/**/*.git')

      repos_to_import.each do |repo_path|
        # strip repo base path
        repo_path[0..git_base_path.length] = ''

        path = repo_path.sub(/\.git$/, '')
        group_name, name = File.split(path)
        group_name = nil if group_name == '.'

        puts "Processing #{repo_path}".yellow

        if path.end_with?('.wiki')
          puts " * Skipping wiki repo"
          next
        end

        project = Project.find_with_namespace(path)

        if project
          puts " * #{project.name} (#{repo_path}) exists"
        else
          user = User.find_by_username('root')

          project_params = {
            name: name,
            path: name
          }

          # find group namespace
          if group_name
            group = Namespace.find_by(path: group_name)
            # create group namespace
            unless group
              group = Group.new(:name => group_name)
              group.path = group_name
              group.owner = user
              if group.save
                puts " * Created Group #{group.name} (#{group.id})".green
              else
                puts " * Failed trying to create group #{group.name}".red
              end
            end
            # set project group
            project_params[:namespace_id] = group.id
          end

          project = Projects::CreateService.new(user, project_params).execute

          puts project.inspect

          if project.valid?
            puts project.errors.inspect
            puts " * Created #{project.name} (#{repo_path})".green
          else
            puts " * Failed trying to create #{project.name} (#{repo_path})".red
            puts "   Validation Errors: #{project.errors.messages}".red
          end
        end
      end

      puts "Done!".green
    end
  end
end
